import discord

client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

@client.event
async def on_message(msg):
    if msg.content == "!dl":
        with open("channel_messages.txt", "a+", encoding="utf-8") as f:
            async for message in msg.channel.history(limit=500000):
                f.write(message.content)
                f.write("\n")
            f.flush()

f = open("token.txt", "r")
token = f.read()
f.close()
client.run(token)

