import random
import discord
import numpy as np
import tensorflow as tf
from tensorflow import keras

SEQ_LENGTH = 20

f = open("formatted_messages.txt", 'rb')
text = f.read().decode(encoding='utf-8')
f.close()
short_lines = text.split('\n')

lines = []
for line in short_lines:
    if len(line) > SEQ_LENGTH:
        lines.append(line)

# Is a set of all pairs of letters found in text
dictionary = set()
prev_let = lines[0][0]
for line in lines:
    for let in line:
        dictionary.add(prev_let + let)
        prev_let = let
dictionary = sorted(dictionary)

# Is a map from indecies to letter pairs and vice versa
let_to_index = dict((c, i) for i, c in enumerate(dictionary))
index_to_let = dict((i, c) for i, c in enumerate(dictionary))

model = keras.models.load_model('model/final-3')

def sample(preds, temperature=1.0):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

def generate_message(length, temperature):
    starting = ''
    while len(starting) < SEQ_LENGTH:
        starting = lines[random.randint(0, len(lines))]
    if len(starting) > SEQ_LENGTH:
        starting = starting[:SEQ_LENGTH]
    print("Starting text:", starting)
    generated = ''
    generated += starting
    for i in range(length):
        x_predictions = np.zeros((1, int(SEQ_LENGTH/2), len(dictionary)))
        for offset in range(0, int(SEQ_LENGTH/2), 2):
            pair = generated[-SEQ_LENGTH + offset] + generated[-SEQ_LENGTH + offset + 1]
            x_predictions[0, int(offset/2), let_to_index[pair]] = 1

        predictions = model.predict(x_predictions, verbose=0)[0]
        next_index = sample(predictions, temperature)
        next_pair = index_to_let[next_index]

        generated += next_pair
    print("Generated:", generated)
    return generated


client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

@client.event
async def on_message(msg):
    if msg.content == "!gen":
        message = generate_message(50, random.random())
        await msg.channel.send("Starting: " + message[:SEQ_LENGTH])
        await msg.channel.send("Generated: " + message)
        # with open("channel_messages.txt", "a+", encoding="utf-8") as f:
        #     async for message in msg.channel.history(limit=500000):
        #         f.write(message.content)
        #         f.write("\n")
        #     f.flush()

f = open("token.txt", "r")
token = f.read()
f.close()
client.run(token)

