
# text = open("channel_messages.txt", 'rb').read().decode(encoding='utf-8')

with open("formatted_messages.txt", "w") as f:
    for line in open("channel_messages.txt", 'rb'):
        text = line.strip()
        if len(text) == 0:
            continue
        if text.startswith(b"!"):
            continue
        if text.startswith(b"http"):
            continue
        if not text.isascii():
            continue
        f.write(text.decode(encoding='ascii'))
        f.write('\n')
