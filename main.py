import random
import operator
import numpy as np
import tensorflow as tf

SEQ_LENGTH = 20
SEQ_STEP = 3

# filepath = tf.keras.utils.get_file('shakespeare.txt', 'https://storage.googleapis.com/download.tensorflow.org/data/shakespeare.txt')

f = open("formatted_messages.txt", 'rb')
text = f.read().decode(encoding='utf-8')
f.close()
short_lines = text.split('\n')

lines = []
for line in short_lines:
    if len(line) > SEQ_LENGTH:
        lines.append(line.lower())

# Values is a list of all SEQ_LENGTH+2 sections found in lines
# Each element is split into one section of length SEQ_LENGTH, and two characters, which are used in training data
values = []
print("Converting", len(lines), "lines into usable values")
for line in lines:
    for i in range(0, len(line), SEQ_STEP):
        if i + SEQ_LENGTH + 2 > len(line):
            break
        val = line[i:i+SEQ_LENGTH+2]
        values.append(val)
print("Generated", len(values), "usable values")

# A map of every pair of letters to the number of times they occur in the values
occurences = {}
print("Finding the number of times every single pair of letters appears in values")
for val in values:
    for i in range(0, SEQ_LENGTH+2, 2):
        pair = val[i] + val[i+1]
        if pair not in occurences:
            occurences[pair] = 0
        occurences[pair] += 1

sorted_occurences = sorted(occurences.items(), key=lambda kv: kv[1])
sorted_occurences.reverse()
print("Got", len(sorted_occurences), "pairs")

# Is a set of all pairs of letters found in text
dictionary = set()
# We only grab the top 75%, as the bottom 25% are pairs that are only used once or twice
for i in range(int(len(sorted_occurences) * 0.5)):
    dictionary.add(sorted_occurences[i][0])

dictionary_list = sorted(dictionary)
print("Removed lower half, and now have a dictionary of", len(dictionary), "items")

# Is a map from indecies to letter pairs and vice versa
let_to_index = dict((c, i) for i, c in enumerate(dictionary_list))
index_to_let = dict((i, c) for i, c in enumerate(dictionary_list))

x = np.zeros((len(values), int(SEQ_LENGTH/2), len(dictionary)), dtype=np.bool)
y = np.zeros((len(values), len(dictionary)), dtype=np.bool)
print("Before pruning values, x shape is:", x.shape)

i = 0
print("Pruning values...")
for val in values:
    completed = True
    for j in range(0, SEQ_LENGTH, 2):
        pair = val[j] + val[j+1]
        if pair not in dictionary:
            completed = False
            break
    if completed:
        pair = val[SEQ_LENGTH] + val[SEQ_LENGTH+1]
        if pair not in dictionary:
            continue
        y[i, let_to_index[pair]] = 1
        for j in range(0, SEQ_LENGTH, 2):
            pair = val[j] + val[j+1]
            x[i, int(j/2), let_to_index[pair]] = 1
        i += 1

x = x[:i]
y = y[:i]
print("After pruning values, x shape is:", x.shape)

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.LSTM(256, input_shape=(int(SEQ_LENGTH/2), len(dictionary))))
model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(len(dictionary)))
model.add(tf.keras.layers.Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer=tf.keras.optimizers.RMSprop(lr=0.0005))

model.fit(x, y, epochs=10)

# serialize model to JSON
model.save('model/final-5')
print("Saved model to disk")
